# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

add_executable(arm-2d-example-benchmark
    ${PROJECT_SOURCE_DIR}/main.c
)

target_link_libraries(arm-2d-example-benchmark
    arm-2d
    arm-2d-helper
    arm-2d-example-common-benchmark
    arm-2d-example-common-controls
    arm-2d-example-common-platform

    iotsdk-serial-retarget
    $<$<STREQUAL:${ARM2D_EXAMPLE_PLATFORM},corstone-300>:mdh-arm-corstone-300-startup>
    $<$<STREQUAL:${ARM2D_EXAMPLE_PLATFORM},corstone-310>:mdh-arm-corstone-310-startup>
)

if(ARM2D_EXAMPLE_TFM)
    if("${ARM2D_EXAMPLE_PLATFORM}" STREQUAL corstone-300)
        set(link_script_armclang ${CMAKE_CURRENT_LIST_DIR}/MPS3_CM55_TFM.sct)
        set(link_script_gcc ${CMAKE_CURRENT_LIST_DIR}/MPS3_CM55_TFM.ld)
        set(tf_m_image_addresses 0x10000000 0x11000000 0x01060000)
    elseif("${ARM2D_EXAMPLE_PLATFORM}" STREQUAL corstone-310)
        set(link_script_armclang ${CMAKE_CURRENT_LIST_DIR}/MPS3_CM85_TFM.sct)
        set(link_script_gcc ${CMAKE_CURRENT_LIST_DIR}/MPS3_CM85_TFM.ld)
        set(tf_m_image_addresses 0x11000000 0x11040000 0x010A0000)
    endif()
    set(full_image ${CMAKE_CURRENT_BINARY_DIR}/arm-2d-example-benchmark_merged.elf)
else()
    if("${ARM2D_EXAMPLE_PLATFORM}" STREQUAL corstone-300)
        set(link_script_armclang ${CMAKE_CURRENT_LIST_DIR}/MPS3_CM55_DTCM.sct)
        set(link_script_gcc ${CMAKE_CURRENT_LIST_DIR}/MPS3_CM55_DTCM.ld)
    elseif("${ARM2D_EXAMPLE_PLATFORM}" STREQUAL corstone-310)
        set(link_script_armclang ${CMAKE_CURRENT_LIST_DIR}/MPS3_CM85_ISRAM.sct)
        set(link_script_gcc ${CMAKE_CURRENT_LIST_DIR}/MPS3_CM85_ISRAM.ld)
    endif()
    set(full_image ${CMAKE_CURRENT_BINARY_DIR}/arm-2d-example-benchmark.elf)
endif()

if(CMAKE_C_COMPILER_ID STREQUAL "ARMClang")
    set(linker_script ${link_script_armclang})
    target_link_options(arm-2d-example-benchmark PRIVATE --scatter=${linker_script})
elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")
    set(linker_script ${link_script_gcc})
    target_link_options(arm-2d-example-benchmark PRIVATE -T ${linker_script})
else()
    message(FATAL_ERROR "${CMAKE_C_COMPILER_ID} is not a supported compiler")
endif()
set_target_properties(arm-2d-example-benchmark PROPERTIES LINK_DEPENDS ${linker_script})

if(ARM2D_EXAMPLE_TFM)
    include(SignTfmImage)
    add_dependencies(arm-2d-example-benchmark trusted-firmware-m-build)
    target_elf_to_bin(arm-2d-example-benchmark)
    iotsdk_tf_m_sign_image(arm-2d-example-benchmark)
    iotsdk_tf_m_merge_images(arm-2d-example-benchmark ${tf_m_image_addresses})
endif()

# Automated testing

target_compile_definitions(arm-2d-example-benchmark
    PRIVATE
        $<$<BOOL:${BUILD_TESTING}>:BUILD_TESTING>
)

add_test(
    NAME    arm-2d-example-benchmark
    COMMAND htrun
            --image-path=${full_image}
            --compare-log=${CMAKE_CURRENT_LIST_DIR}/${ARM2D_EXAMPLE_HTRUN_LOG}
            --sync=0 --baud-rate=115200 --polling-timeout=120
            --micro=${ARM2D_EXAMPLE_HTRUN_TARGET} --fm=MPS3
    COMMAND_EXPAND_LISTS
)
