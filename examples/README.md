# README



## 1. Default Examples 



### 1.1 Benchmark

**PROJECT:    benchmark**

It is an **ALL-IN-ONE** example that demonstrates almost all the features provided by the library. It is also used as a benchmark of Arm-2D. Since there is no public benchmark available for micro-controllers, we decide to overcome this problem with the following methods and considerations:

- **Choose the widely used algorithms in embedded GUI as the body of the benchmark**
  - Alpha-blending
  - Colour-Keying
  - Image Copy
  - Texture Paving
  - Rotation
  - Mirroring
  - Masking
- **Simulate a typical application scenario with sufficient complexity**
  - Background with Texture paving (switching different mirroring modes every 4 second)
  - Foreground picture 
  - Two constructed layers for alpha-blending and texture paving
  - Moving icons
  - Spinning busy wheel
- **Choose a typical low-cost LCD resolution 320*240 in RGB565**
- **Let those layers float with different angles and speed to cover a sufficient number of conditions.**
- **Record the cycle count used for blending one frame and run 1000 iterations (frames).** 



**Figure 1-1 A snapshot of benchmark running on Cortex-M4 FVP platform**

![](../documentation/pictures/benchmark)



- **Use the average cycle count in 1000 iterations as benchmark score.**

  - Based on that, for typical embedded application requirement, we derive a more meaningful metrics called the **Minimal Frequency Required for 30 FPS (MHz)** as shown in **Figure 1-5**. 

  

**Figure 1-2 Performance Comparison among some Cortex-M processors**

![image-20210318225839820](../documentation/pictures/TopReadme_1_6_2_b.png)



### 1.2 Watch-Panel

**PROJECT:    watch_panel**

It is an example of rotation APIs. It consists of five layers:

1. A floating background layer (Using Tile Copy)
2. A clock_panel layer (Using Alpha Blending With Colour Keying)
3. A small gold gear (Rotation)
4. A big black gear (Rotation with Opacity)
5. A red pointer (Rotation)
6. A golden star (Rotation, scaling with Opacity)

This demo also shows how a fancy looking clock could be rendered with just less than 10K RAM (or even less by using a smaller PFB size). 

**Figure 1-3  A snapshot of watch-panel running on Cortex-M4 FVP platform**

![](../documentation/pictures/watch-panel)


## 2. Building the examples with CMake

### 2.1 Prerequisites

Please set up your build environment by following
[the prerequisites of the Open IoT SDK](https://gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/Prerequisites.md).
To build the examples, you need either the Arm Compiler for Embedded (ARMClang) or the GNU Arm Embedded Toolchain (GCC).

To run the examples, you need to have the FVP (Fixed Virtual Platforms) or the AVH (Arm Virtual Hardware) for
*Corstone-300* or *Corstone-310* installed. For more information, please refer to
* [Arm Ecosystem FVPs](https://developer.arm.com/downloads/-/arm-ecosystem-fvps)
* [Development Tools and Software - Fixed Virtual Platforms](https://www.arm.com/products/development-tools/simulation/fixed-virtual-platforms)
* [Development Tools and Software - Arm Virtual Hardware](https://www.arm.com/products/development-tools/simulation/virtual-hardware)

### 2.2 Building the examples

Once you have your build environment set up, you can build both the Benchmark and the Watch-Panel examples with your
choice of compiler. To do this, change your working directory to the directory of this document (i.e. `examples` under
the directory where you cloned the repository) and run:

For ARMClang:
```shell
cmake -B __build -GNinja --toolchain toolchains/toolchain-armclang.cmake
cmake --build __build
```

For GCC:
```shell
cmake -B __build -GNinja --toolchain toolchains/toolchain-arm-none-eabi-gcc.cmake
cmake --build __build
```

This builds the default configuration, which
* targets Corstone-300
* enables [TrustedFirmware-M (TF-M)](https://www.trustedfirmware.org/projects/tf-m)
* optimizes emulation on the FVP/AVH by:
  - selecting the tiny mode, in which examples are simplified in order to run at a good frame rate on the FVP/AVH but
    demonstrate fewer features of Arm-2D compared to the full mode.
  - compiling Arm-2D and CMSIS-DSP without MVE, M-Profile Vector Extension, because MVE can greatly benefit performance
    of real hardware but is very slow to emulate on the FVP/AVH due to differences with the host architecture.

To override the default configuration, append to the CMake configuration command (`cmake -B __build -GNinja ...` above)
extra arguments of your choice:
* target Corstone-310: `-D ARM2D_EXAMPLE_PLATFORM=corstone-310`.
* disable TF-M: `-D ARM2D_EXAMPLE_TFM=OFF`.
* select the full mode (at the expense of frame rate on the FVP/AVH): `-D ARM2D_EXAMPLE_TINY_MODE=OFF`. **Note**: The
full mode requires TF-M to be disabled (`-D ARM2D_EXAMPLE_TFM=OFF`) because there is not enough ROM to fit both the full
mode and TF-M.
* enable MVE (at the expense of frame rate on the FVP/AVH): `-D ARM2D_EXAMPLE_MVE=ON`.

### 2.3 Running the examples

Once you have built the examples, you can run them on either an FVP or an AVH. The example images to run depend on
whether TF-M is enabled or not.

#### 2.3.1 With TF-M

If you have built the examples with TF-M enabled, which is *by default*, you can run the Benchmark example as follows,
replacing `<path-to>` with your FVP or AVH's installation path:

```shell
# If using FVP:

## Corstone-300:
/<path-to>/FVP_Corstone_SSE-300_Ethos-U55 -a __build/benchmark/arm-2d-example-benchmark_merged.elf

## Corstone-310:
/<path-to>/FVP_Corstone_SSE-310 -a __build/benchmark/arm-2d-example-benchmark_merged.elf

# If using AVH:

## Corstone-300:
/<path-to>/VHT_Corstone_SSE-300_Ethos-U55 -a __build/benchmark/arm-2d-example-benchmark_merged.elf

## Corstone-310:
/<path-to>/VHT_Corstone_SSE-310 -a __build/benchmark/arm-2d-example-benchmark_merged.elf
```

This opens
* a serial terminal showing boot messages from the bootloader, the TF-M secure firmware and the Arm-2D non-secure
Benchmark application.
* a graphical window showing the Benchmark animation on the emulated LCD.

The suffix `_merged` indicates a merged image containing the bootloader, the TF-M secure firmware and the Arm-2D
non-secure Benchmark application.

Similarly, to run the Watch-Panel example, replace `__build/benchmark/arm-2d-example-benchmark_merged.elf` with
`__build/watch_panel/arm-2d-example-watch-panel_merged.elf` in the commands above.

#### 2.3.2 Without TF-M

If you have built the examples with TF-M disabled, i.e. `-D ARM2D_EXAMPLE_TFM=OFF`, no merged images are generated. You
can run the commands above without the `_merged` suffix in the image names:
* Benchmark: `__build/benchmark/arm-2d-example-benchmark.elf`.
* Watch-Panel: `__build/watch_panel/arm-2d-example-watch-panel.elf`.
