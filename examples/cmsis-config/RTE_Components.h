/*
 * Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H

/**
 * CMSIS configuration file.
 * Note: Keil MDK automatically creates RTE_Components.h, but when using CMake
 * we need to provide this file manually.
 */

#endif // RTE_COMPONENTS_H
