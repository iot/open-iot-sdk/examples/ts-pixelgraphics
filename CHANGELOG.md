# v2023.04 (2023-04-13)

## Changes

* ci: Enable possible tpip violation warning messages in Merge Requests.
  ci: Fix job name to reflect the newest changes in the developer-tools.
